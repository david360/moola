import {useEffect, useState} from "react";
import {IMappedCategoryWithExpense} from "./types";
import {defaultCategory, expensesCategories, SVG_EXTENSION} from "./constants";
import {isMomentMonthsEqual} from "../helpers/momentFunctions";
import moment from "moment";
import {useProgressState} from "./CustomHooks/useProgressState";

export const useMappedSpendingLimit = (selectedDate,expenses,spendingTargets) => {
    const {success, setSuccess, setLoading} = useProgressState();
    const [mappedCategoriesWithExpenses, setMappedCategoriesWithExpenses] = useState<IMappedCategoryWithExpense[]>([]);
    const mappedExpensesCategories = expensesCategories.reduce((acc, ex) => ({
        ...acc,
        [ex.id]: {...ex, image: ex.name + SVG_EXTENSION}
    }), {});
    useEffect(() => {
        if (selectedDate) {
            setLoading();
            const mappedExpenses = expenses.reduce((acc, ex) => {
                const oldEx = acc[ex.categoryId];
                if (oldEx) {
                    return {...acc, [ex.categoryId]: {...oldEx, expense: oldEx.expense + (+ex.amount)}}
                } else {
                    const thisMonthLimit = spendingTargets
                        .filter(item => isMomentMonthsEqual(moment(item.createdAt), moment(selectedDate)) && item.categoryId === ex.categoryId)
                        .sort((a, b) => a.createdAt - b.createdAt);
                    let limit = 0;
                    if (thisMonthLimit.length > 0) {
                        limit = +thisMonthLimit[thisMonthLimit.length - 1].amount;
                    } else {
                        const oldMonthLimits = spendingTargets
                            .filter(item => item.categoryId === ex.categoryId && item.createdAt < ex.createdAt)
                            .sort((a, b) => a.createdAt - b.createdAt);
                        if (oldMonthLimits.length > 0) {
                            limit = +oldMonthLimits[oldMonthLimits.length - 1].amount
                        }
                    }
                    return {...acc, [ex.categoryId]: {expense: +ex.amount, limit}};
                }
            }, {});
            const temp = spendingTargets
                .sort((a, b) => a.createdAt - b.createdAt)
                .reduce((acc, st) => {
                    if (mappedExpenses[st.categoryId] && mappedExpenses[st.categoryId].expense) {
                        return {
                            ...acc, [st.categoryId]: {
                                ...mappedExpenses[st.categoryId],
                                ...(mappedExpensesCategories[st.categoryId] || defaultCategory)
                            }
                        };
                    } else if (isMomentMonthsEqual(moment(st.createdAt), moment(selectedDate))) {
                        return {...acc,[st.categoryId]: {
                                expense: 0,
                                limit: st.amount,
                                ...(mappedExpensesCategories[st.categoryId] || defaultCategory)
                            }}
                    }
                    return acc;
                }, {});
            const temp2 = Object.keys(mappedExpenses).filter(key => !Object.keys(temp).includes(key)).reduce((acc,key) => {
                return {...acc,[key]: {...mappedExpenses[key], ...(mappedExpensesCategories[key] || defaultCategory)}}
            },{});
            setMappedCategoriesWithExpenses(Object.values({...temp,...temp2}));
            setTimeout(setSuccess);
        }
    }, [selectedDate, spendingTargets, expenses]);
    return {
        success,
        mappedCategoriesWithExpenses,
    }
};
