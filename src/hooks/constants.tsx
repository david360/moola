import moment from "moment";
import {ICategory} from "./types";

export const CATEGORIES_FILENAME = 'categories.json';
export const SPENDING_TARGET_FILENAME = 'spendingTarget.json';
export const EXPENSES_FILENAME = 'expenses.json';
export const INCOMES_FILENAME = 'incomes.json';
export const CURRENCY_FILENAME = 'currency.json';

export const maxDate = moment();
export const minDate = moment('01/01/2019', 'DD/MM/YYYY').subtract(1, 'ms');

export const expensesCategories: ICategory[] = [
    {
        id: '1',

        image: '', name: 'food',
    },
    {
        id: '2',

        image: '', name: 'bills',
    },
    {
        id: '3',

        image: '', name: 'transportation'
    },
    {
        id: '4',

        image: '', name: 'home'
    },
    {
        id: '5',

        image: '', name: 'car',
    },
    {
        id: '6',

        image: '', name: 'entertainment'
    },
    {
        id: '7',

        image: '', name: 'clothing',
    },
    {
        id: '8',

        image: '', name: 'insurance'
    },
    {
        id: '9',

        image: '', name: 'tax'
    },
    {
        id: '10',
        image: '',
        name: 'health'
    },
    {
        id: '11',
        image: '',
        name: 'baby'
    },
    {
        id: '12',
        image: '',
        name: 'pet'
    },
    {
        id: '13',
        image: '',
        name: 'beauty'
    },
    {
        id: '14',
        image: '',
        name: 'electronics'
    },
    {
        id: '15',
        image: '',
        name: 'grocery'
    },
    {
        id: '16',
        image: '',
        name: 'gift'
    },
    {
        id: '17',
        image: '',
        name: 'donation'
    },
    {
        id: '18',
        image: '',
        name: 'travel'
    },
    {
        id: '19',
        image: '',
        name: 'stationary'
    },
    {
        id: '20',
        image: '',
        name: 'work'
    },
    {
        id: '21',
        image: '',
        name: 'others'
    },
];

export const defaultCategory: ICategory =
    {
        id: '21',
        image: 'others.svg',
        name: 'others'
    };

export const SVG_EXTENSION = '.svg';


export const incomeCategories: ICategory[] = [
    {
        id: '1',
        image: '',
        name: 'salary'
    },
    {
        id: '2',
        image: '',
        name: 'awards'
    },
    {
        id: '3',
        image: '',
        name: 'commission'
    },
    {
        id: '4',
        image: '',
        name: 'investments'
    },
    {
        id: '5',
        image: '',
        name: 'refunds'
    },
    {
        id: '6',
        image: '',
        name: 'others'
    },
];