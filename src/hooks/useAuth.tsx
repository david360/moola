import {useEffect, useState} from "react";
import {UserSession, AppConfig, loadUserData} from 'blockstack';
import {notify} from "react-notify-toast";
import {IUser} from "./types";

export const useAuth = () => {
    const [isAuthenticated, setAuthenticated] = useState<boolean | undefined>(undefined);
    const [loading, setLoading] = useState<boolean>(false);
    const [userData, setUserData] = useState<IUser | undefined>(undefined);
    const userSession = new UserSession({
        appConfig: new AppConfig(['store_write', 'publish_data'])
    });
    useEffect(() => {
        const fetch = async () => {
            try {
                if (!userSession.isUserSignedIn() && userSession.isSignInPending()) {
                    setLoading(true);
                    await userSession.handlePendingSignIn();
                    setUserData(loadUserData());
                    setAuthenticated(userSession.isUserSignedIn());
                    setLoading(false);
                }
                if (userSession.isUserSignedIn()) {
                    const loadedUserData = loadUserData();
                    setLoading(true);
                    if (loadedUserData) {
                        setUserData(loadedUserData);
                        setLoading(false);
                    } else {
                        localStorage.clear();
                        location.reload();
                    }
                }
            } catch (e) {
                notify.show("Something went wrong!", "error");
                localStorage.clear();
                location.reload();
            }
        };
        fetch();
    }, []);
    useEffect(() => {
        setAuthenticated(userSession.isUserSignedIn());
    });
    const handleSignIn = async () => {
        await userSession.redirectToSignIn();
    };
    const handleLogOut = () => {
        userSession.signUserOut();
        localStorage.clear();
        location.reload();
    };
    return {
        handleSignIn,
        handleLogOut,
        isAuthenticated,
        loading,
        userData,
        setUserData,
    }
};
