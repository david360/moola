import {ICrud} from "./useItemsCrud";


export interface IUser {
    username: string;
    profile: {
        description?: string;
        image: Array<{ contentUrl: string, name: string }>;
        name: string;
    }
}


export interface ICategory {
    name: string;
    image: string;
    id: string;
}


export interface IAddSpendingTarget {
    amount: string;
    categoryId: string;
}

export interface IEditSpendingTarget {
    amount: string;
}

export interface ISpendingTarget extends ICrud, IAddSpendingTarget {
}


export interface IAddExpense {
    amount: string;
    categoryId: string;
    description?: string;
}

export interface IEditExpense {
    amount?: string;
    categoryId?: string;
    description?: string;
}
export interface IExpense extends ICrud, IAddExpense {
}


export interface IAddIncome {
    amount: string;
    categoryId: string;
    description?: string;
}

export interface IEditIncome {
    amount?: string;
    categoryId?: string;
    description?: string;
}
export interface IIncome extends ICrud, IAddIncome {
}


export interface IMappedCategoryWithExpense extends ICategory {
    expense: number;
    limit: number;
}

export interface IIncomeTransactionWithCategory extends IIncome {
    name: string;
    image: string;
    isIncome?: boolean;
}

export interface IExpenseTransactionWithCategory extends IExpense {
    name: string;
    image: string;
    isIncome?: boolean;
}


export const DAY = 'day';
export type day = typeof DAY;
export const MONTH = 'month';
export type month = typeof MONTH;