import {useEffect, useRef, useState} from "react";
import {useProgressState} from "./CustomHooks/useProgressState";
import {getFile, putFile} from 'blockstack';
import uuid from 'uuid/v4';
import moment, {Moment} from "moment";
import {isMomentMonthsEqual} from "../helpers/momentFunctions";


export interface ICrud {
    id: string;
    createdAt: number;
    updatedAt: number;
}

export function useItemsCrud<T>(FILENAME: string, selectedDate?: Moment) {
    const [items, setItems] = useState<Array<ICrud & T>>([]);
    const [filteredItems, setFilteredItems] = useState<Array<ICrud & T>>([]);
    const {success, failure, setFailure, setLoading, setSuccess} = useProgressState();
    const itemsRef = useRef(items);
    itemsRef.current = items;
    useEffect(() => {
        if(selectedDate){
            setFilteredItems(itemsRef.current.filter(item => isMomentMonthsEqual(moment(item.createdAt),moment(selectedDate))));
        }
    },[selectedDate,itemsRef.current]);
    useEffect(() => {
        setLoading();
        getFile(FILENAME).then((res: any) => {
            setItems(JSON.parse(res) || []);
            setSuccess();
        }).catch(setFailure);
    }, []);
    const addItem = (item: T) => {
        const now = +moment(selectedDate).format('x');
        const id = uuid().replace('-', '');
        const newItem = {
            ...item,
            id,
            createdAt: now,
            updatedAt: now,
        };
        const newItems = [...itemsRef.current, newItem];
        return putFile(FILENAME, JSON.stringify(newItems)).then(() => {
            setItems(newItems);
        })
    };
    const deleteItem = (item: ICrud & T) => {
        const newItems = itemsRef.current.filter(c => c.id !== item.id);
        return putFile(FILENAME, JSON.stringify(newItems)).then(() => {
            setItems(newItems);
        });
    };
    const updateItem = (item: ICrud & T, newValues: T) => {
        const newItems = itemsRef.current.map(c => {
            if (c.id === item.id) {
                return ({...c, ...newValues});
            }
            return c;
        });
        return putFile(FILENAME, JSON.stringify(newItems)).then(() => {
            setItems(newItems);
        });
    };
    return {
        addItem,
        deleteItem,
        updateItem,
        items,
        filteredItems,
        success,
        failure,
    }
};