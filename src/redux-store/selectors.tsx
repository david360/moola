import {IFormState, IRootReducerState} from "./rootReducer";

export const selectForm = (state: IRootReducerState): IFormState => state.form;
