import {all} from "redux-saga/effects";
import * as SAGAS from './sagas';

export default function* rootSaga() {
  yield all([
    SAGAS.watchPassOnFormValuesSaga(),
  ])
}
