export const transformPhone = (str: string) => str && str.replace(/\D/g, '');
