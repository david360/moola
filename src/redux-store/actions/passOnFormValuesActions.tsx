
export interface IPassOnFormValuesAction {
  type: typeof ACTION_PASS_ON_FORM_VALUES;
  payload: (formValues) => void;
}
export const ACTION_PASS_ON_FORM_VALUES = 'ACTION_PASS_ON_FORM_VALUES';

export const passOnFormValues = (fn): IPassOnFormValuesAction => ({
    type: ACTION_PASS_ON_FORM_VALUES,
    payload: fn,
});
