import {PersistConfig, persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import {combineReducers} from 'redux';
import {FormState, reducer as form} from 'redux-form';

export interface IFormState {
  addCategoryForm: FormState;
}

export interface IRootReducerState {
  form: IFormState;
}

const rootReducer = combineReducers<(Action) => IRootReducerState>({
  form,
});

const persistorOptions: PersistConfig = {
  key: 'root',
  storage,
  whitelist: [
    'appReducer',
  ]
};

const persistedReducer = persistReducer(persistorOptions, rootReducer);

export default persistedReducer;
