import * as React from "react";


export type ISetCurrency = (currency: string) => void;

interface IContextValues {
  currency: string | undefined,
  setCurrency: ISetCurrency;
}

export default React.createContext<IContextValues>({
  currency: undefined,
  setCurrency: (themeData) => undefined,
})
