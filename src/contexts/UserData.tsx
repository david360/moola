import * as React from "react";
import {IUser} from "../hooks/types";



export type ISetUserData = (userData: IUser) => void;

interface IContextValues {
  userData: IUser | undefined,
  setUserData: ISetUserData;
}

export default React.createContext<IContextValues>({
  userData: undefined,
  setUserData: (themeData) => undefined,
})
