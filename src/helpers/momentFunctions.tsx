import {default as moment, Moment} from "moment";

export const isMomentMonthsEqual = (m1: Moment, m2: Moment): boolean =>
    (m1.month() === m2.month()) && (m1.year() === m2.year());
export const isMomentDaysEqual = (m1: Moment, m2: Moment): boolean =>
    (m1.date() === m2.date()) && (m1.month() === m2.month()) && (m1.year() === m2.year());

export const immutableMoment = (m: Moment) => moment(m);


export const getMaxDay = (m: Moment) => {
    if ((m.year() === moment().year()) && (m.month() === moment().month())) {
        return moment();
    } else {
        return m.endOf('month');
    }
};