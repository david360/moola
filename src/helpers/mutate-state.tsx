import {Map} from 'immutable'
export function mutateState<T, K, V>(map: T, mutator: (map: Map<K, V>) => any): T {
  return Map(map).withMutations(mutator).toJS();
}
