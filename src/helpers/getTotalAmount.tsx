import {useContext} from "react";
import CurrencyContext from "../contexts/CurrencyContext";
import getSymbolFromCurrency from 'currency-symbol-map'

export const getTotalAmount = (arr: any) => arr.reduce((acc, i) => (acc + (+i.amount)), 0);
export const useNumberWithCommas = (x) => {
    const {currency} = useContext(CurrencyContext);
    return getSymbolFromCurrency(currency) + x.toLocaleString('en');
};