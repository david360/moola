import {IUser} from "../hooks/types";
export const convertDataURLToBase64 = (url: string): Promise<string> => {
    const isDataUrl = url.slice(0, 4) === 'data';
    const urlTrail = isDataUrl ? '' : '?_=' + new Date().getTime();
    return fetch(url + urlTrail)
        .then(res => res.blob())
        .then(async (blob) => {
            return await readAsBase64(new File([blob], "File name", {type: 'image'}));
        });
};
export const readAsBase64 = (file: File): Promise<string> => {
    return new Promise(resolve => {
        const reader = new FileReader();
        reader.onload = (ev: any) => resolve(ev.target.result);
        reader.readAsDataURL(file);
    })
};

export const getUserImage = async (userData: IUser) => {
    const isUserImageAvailable = userData && userData.profile && userData.profile.image && userData.profile.image.length > 0
        && userData.profile.image.filter((item) => item.name === 'avatar').length > 0;
    if (isUserImageAvailable) {
        const imageUrl = userData.profile.image.filter((item) => item.name === 'avatar')[0].contentUrl;
        const userImageRes = await convertDataURLToBase64(imageUrl);
        if (userImageRes) {
            return userImageRes;
        }
        return require('../assets/default-user.svg');
    }
    return require('../assets/default-user.svg');
};