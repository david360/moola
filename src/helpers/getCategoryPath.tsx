
export const getExpensesCategoryPath = (name:string) => require(`../assets/expensesCategories/${name}`);
export const getIncomeCategoryPath = (name:string) => require(`../assets/incomeCategories/${name}`);