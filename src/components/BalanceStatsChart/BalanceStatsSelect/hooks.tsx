import { IBalanceStatsSelect, ChartTypes, chartTypesSelectItems } from ".";

import { useMemo, useState, useEffect, useCallback } from "react";
import {IDropDownItemData} from "../../FormElements/select";

const noop = () => null;

export const useSelect = (onChange?: IBalanceStatsSelect['onChange'], value?: ChartTypes) => {
  const handleChangeExternal = useMemo(() => onChange || noop, [onChange]);
  const [selectedChartType, setSelectedChartType] = useState(chartTypesSelectItems[0]);

  useEffect(() => {
    handleChange(chartTypesSelectItems.find(({ value: v }) => v === value || v === selectedChartType.value) as IDropDownItemData<ChartTypes>);
  }, [value]);

  const handleChange = useCallback((newValue: IDropDownItemData<ChartTypes>) => {
    if (newValue.value !== selectedChartType.value) {
      setSelectedChartType(newValue);
      handleChangeExternal(newValue);
    }
  }, [handleChangeExternal, setSelectedChartType, selectedChartType]);
  return { handleChange, selectedChartType };
};
