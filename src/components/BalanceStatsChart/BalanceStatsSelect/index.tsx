import { useSelect } from "./hooks";
import { motion, AnimatePresence, Variants } from 'framer-motion';
import React, { useCallback } from 'react'
import * as styles from './styles.module.sass';
import Arrow from '@material-ui/icons/KeyboardArrowDown'
import {useToggleState} from "../../../hooks/CustomHooks/useToggleState";
import {IDropDownItemData} from "../../FormElements/select";


const list = {
  visible: {
    opacity: 1,
    transition: {
      when: "beforeChildren",
      staggerChildren: 0.3,
    },
  },
  hidden: {
    opacity: 0,
    transition: {
      when: "afterChildren",
    },
  },
};

const listItem: Variants = {
  visible: {
    opacity: 1,
    transition: {
      duration: 0.2,
    },
    y: 0,
  },
  hidden: {
    opacity: 0,
    transition: {
      duration: 0.2,
    },
    y: 20,
  },
};

export enum ChartTypes {
  Week1 = 'First Week',
  Week2 = 'Second Week',
  Week3 = 'Third Week',
  Week4 = 'Fourth Week',
  Month = 'Month',
}

export const chartTypesSelectItems: Array<IDropDownItemData<ChartTypes>> = [
  {
    label: 'Month',
    value: ChartTypes.Month,
  },
  {
    label: ChartTypes.Week1,
    value: ChartTypes.Week1,
  },
  {
    label: ChartTypes.Week2,
    value: ChartTypes.Week2,
  },
  {
    label: ChartTypes.Week3,
    value: ChartTypes.Week3,
  },
  {
    label: ChartTypes.Week4,
    value: ChartTypes.Week4,
  },
];

export interface IBalanceStatsSelect {
  // options: IDropDownItemData[];
  value?: ChartTypes;
  onChange?: (chartType: IDropDownItemData<ChartTypes>) => void;
}

const { div: Div } = motion;

const ListItem: React.FC<{ item: IDropDownItemData; onSelect: (item: IDropDownItemData) => void }> = ({
  item,
  onSelect
}) => {
  const handleSelect = useCallback(() => onSelect(item), [item.value]);
  return (
    <Div
      onClick={handleSelect}
      whileTap={{ scale: 0.9 }}
      className={styles.selectItem}
      initial='hidden'
      exit='hidden'
      whileHover={{ backgroundColor: '#ebebeb', }}
      animate='visible'
      variants={listItem}
      key={item.value}>
      {item.label}
    </Div>
  )
}

const BalanceStatsSelect: React.FC<IBalanceStatsSelect> = ({
  onChange,
  // options,
  value,
}) => {
  const { selectedChartType, handleChange } = useSelect(onChange, value);
  const [isOpen, toggle] = useToggleState();

  const handleSelect = (item: IDropDownItemData<ChartTypes>) => {
    toggle();
    handleChange(item);
  }
  return (
    <Div className={styles.wrapper}>
      <Div className={styles.toggle} onClick={toggle}>
        <Div className={styles.selectedLabel}>
          {selectedChartType.label}
        </Div>
        <Div animate={{ rotate: isOpen ? '180deg' : '0deg' }}>
          <Arrow className={styles.arrow} />
        </Div>
      </Div>
      <AnimatePresence>
        {
          isOpen &&
          <Div
            variants={list}
            onClick={toggle}
            initial='hidden'
            exit='hidden'
            key='BalanceStatsSelect'
            animate='visible'
            transition={{ duration: 0.4 }}
            className={styles.menu}>
            {
              chartTypesSelectItems.map((a) => (<ListItem item={a} key={`BalanceStatsSelect-valuel-${a.value}`} onSelect={handleSelect} />))
            }
          </Div>
        }
      </AnimatePresence>
    </Div>
  );
};

export default BalanceStatsSelect;
