import Chart from 'react-apexcharts';
import React, {useEffect, useMemo, useState} from 'react';
import * as styles from './styles.module.sass';
import BalanceStatsSelect, {ChartTypes} from './BalanceStatsSelect';
import moment, {Moment} from 'moment';
import {ApexOptions} from 'apexcharts';
import {IDropDownItemData} from "../FormElements/select";

// const DAYS = (selectedMonth) => {
//   const days: any = [];
//   const dateStart = moment(selectedMonth).startOf('month');
//   for (let i = 0 ; i< moment(selectedMonth).daysInMonth(); i++){
//     days.push(moment(dateStart).add(i,'day'));
//   }
//   return days
// };
const chartOptions = (min: number, max: number): ApexOptions => ({
    // tooltip: {
    //   enabled: false,
    // },
    plotOptions: {
        bar: {
            horizontal: false,
            columnWidth: '20%',
            colors: {
                ranges: [{
                    from: 0,
                    to: Infinity,
                    color: '#B5BBC1'
                }]
            },
        },
    },
    grid: {
        yaxis: {
            lines: {show: false}
        },
    },
    yaxis: {
        min: 0,
        labels: {
            style: {
                cssClass: styles.yAxisLabel
            },
        },
        tickAmount: 5,
    },
    dataLabels: {
        enabled: false,
    },
    xaxis: {
        min,
        max,
        type: 'datetime',
        floating: false,
        axisTicks: {
            height: 0,
        },
        axisBorder: {
            color: 'rgba(181,187,183,.5)',
            height: 1.5,
        } as any,
        labels: {
            style: {
                cssClass: styles.xAxisLabel
            }
        }
    },
    tooltip: {
        // enabled: false,
        y: {
            formatter: (val) => {
                return val.toLocaleString('en') + " Balance"
            }
        }
    },
    fill: {
        opacity: 1
    },
});


const BalanceStatChart: React.FC<{ chartSeries: any, selectedMonth: Moment }> = ({chartSeries, selectedMonth}) => {
    const [filteredChartSeries, setFilteredChartSeries] = useState(chartSeries);
    const [min, setMin] = useState(chartSeries);
    const [max, setMax] = useState(chartSeries);
    const [selectedChartType, setSelectedChartType] = useState<ChartTypes>(ChartTypes.Month);
    useEffect(() => {
        const tempMin = +moment(selectedMonth).startOf('month').format('x');
        const tempMax = +moment(selectedMonth).endOf('month').format('x');
        switch (selectedChartType) {
            case ChartTypes.Month:
                setFilteredChartSeries(chartSeries);
                setMin(tempMin || undefined);
                setMax( tempMax|| undefined);
                break;
            case ChartTypes.Week1:
                setFilteredChartSeries(chartSeries.slice(0, 7));
                setMin(+moment(selectedMonth).startOf('month').format('x') || undefined);
                setMax(+moment(tempMin).add(7, 'day') || undefined);
                break;
            case ChartTypes.Week2:
                setFilteredChartSeries(chartSeries.slice(8, 14));
                setMin(+moment(tempMin).add(7, 'day') || undefined);
                setMax(+moment(tempMin).add(14, 'day') || undefined);
                break;
            case ChartTypes.Week3:
                setFilteredChartSeries(chartSeries.slice(15, 21));
                setMin(+moment(tempMin).add(14, 'day') || undefined);
                setMax(+moment(tempMin).add(21, 'day') || undefined);
                break;
            case ChartTypes.Week4:
                setFilteredChartSeries(chartSeries.slice(22));
                setMin(+moment(tempMin).add(21, 'day') || undefined);
                setMax(tempMax || undefined);
                break;
        }
    }, [selectedChartType, chartSeries, selectedMonth]);
    useEffect(() => {
        setSelectedChartType(ChartTypes.Month);
    }, [selectedMonth]);
    const handleChangeChart = (item: IDropDownItemData<ChartTypes>) => {
        setSelectedChartType(item.value);
    };
    const memoizedChartOptions = useMemo(() => chartOptions(min, max), [min, max]);
    return (
        <div className={styles.wrapper}>
            <div className={styles.chartHeader}>
                <p>Balance Overview</p>
                <BalanceStatsSelect value={selectedChartType} onChange={handleChangeChart}/>
            </div>
            <Chart type='area'
                   options={memoizedChartOptions}
                   series={[{
                       name: "Balance",
                       data: filteredChartSeries || []
                   }]}
                   height="280"
            />
        </div>
    )
};

export default BalanceStatChart;
