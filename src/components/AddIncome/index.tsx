import * as React from "react";
import * as styles from './styles.module.sass';
import {hot} from "react-hot-loader";
import {Modal, ModalBody} from "reactstrap";
import {Dispatch} from "redux";
import {passOnFormValues as passOnFormValuesRequest} from "../../redux-store/actions";
import {Field, InjectedFormProps, reduxForm} from "redux-form";
import {connect} from "react-redux";
import {FormEvent, useEffect} from "react";
import {LoadingPageInfo} from "../LoadingPageInfo";
import {isEmpty, required} from "../../redux-store/validators";
import {useProgressState} from "../../hooks/CustomHooks/useProgressState";
import {CustomSelect} from "../FormElements/select";
import {RenderGenericField} from "../FormElements/RenderGenericField";
import {IAddIncome, ICategory, IEditIncome, IIncome} from "../../hooks/types";
import {RenderTextAreaGenericField} from "../FormElements/RenderTextAreaGenericField";
import {useNumberWithCommas} from "../../helpers/getTotalAmount";
import {incomeCategories} from "../../hooks/constants";

export interface IDispatchProps {
    passOnFormValues: (fn) => void
}

interface IProps extends InjectedFormProps, IDispatchProps {
    modal: boolean;
    categories: ICategory[];
    updateIncome: (income: IIncome, newValues: IEditIncome) => Promise<any>;
    selectedIncome: IIncome;
    toggle: () => void;
    addIncome: (income: IAddIncome) => Promise<any>;
}

const AddIncome: React.FC<IProps> = ({
                                         toggle, modal, addIncome, valid,selectedIncome,
                                         dirty, passOnFormValues, touch, categories,updateIncome,initialize
                                     }) => {
    const {setFailure, setSuccess, setLoading, loading} = useProgressState();
    useEffect(() => {
        const initializeForm = async () => {
            if (selectedIncome) {
                const selectedCategory = incomeCategories.filter(c => c.id === selectedIncome.categoryId.toString());
                initialize({...selectedIncome,category: selectedCategory[0]});
            }
        };
        initializeForm();
    }, [selectedIncome]);
    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();
        touch(
            'amount',
            'category',
            'description',
        );
        if (valid && dirty) {
            setLoading();
            passOnFormValues(async ({addIncomeForm}) => {
                const {category, amount,description} = addIncomeForm.values;
                if(selectedIncome){
                    updateIncome(selectedIncome,{categoryId: category.id, amount,description}).then(() => {
                        setSuccess();
                        toggle();
                    }).catch(setFailure);
                }else{
                    addIncome({categoryId: category.id, amount,description}).then(() => {
                        setSuccess();
                        toggle();
                    }).catch(setFailure);
                }
            });
        }
    };
    return (
        <Modal isOpen={modal} toggle={toggle} centered={true} contentClassName={styles.content}
               modalClassName={styles.outline}>
            <ModalBody className={styles.body}>
                <div  className={styles.container}>
                    <Field
                        name='amount'
                        type='number'
                        icon={<p style={{fontSize: 58, fontWeight: 900, color: '#23D796'}}>+</p>}
                        placeholder={useNumberWithCommas('00.00')}
                        component={RenderGenericField as any}
                        validate={[required, isEmpty]}
                    />
                    <div className={styles.separator}/>
                    <Field
                        name='description'
                        type='text'
                        placeholder='Description (Optional)'
                        component={RenderTextAreaGenericField as any}
                    />
                    <div className={styles.separator}/>
                    <Field
                        name='category'
                        rowItems={3}
                        isIncome={true}
                        component={CustomSelect as any}
                        options={categories}
                        validate={[required]}
                    />
                    <button disabled={loading} onClick={handleSubmit} className={styles.btn}>
                        {loading ? <>
                            <p>Saving</p>
                            <LoadingPageInfo color='white' size={4} disabledClass={true}/>
                        </> : '+ Record Transaction'}
                    </button>
                </div>
            </ModalBody>
        </Modal>
    );
};
const mapDispatchToProps = (dispatch: Dispatch) => ({
    passOnFormValues: (fn) => dispatch(passOnFormValuesRequest(fn)),
});

const AddIncomeForm = reduxForm({
    form: 'addIncomeForm',
})(AddIncome as any) as any;

export default hot(module)(connect(null, mapDispatchToProps)(AddIncomeForm) as any);
