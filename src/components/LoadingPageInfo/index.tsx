import {GridLoader} from "react-spinners";
import React from "react";
import * as styles from './styles.module.sass';

interface IProps {
  size?: number;
  color?: string;
  disabledClass?: boolean;
}

export const LoadingPageInfo:React.FC<IProps> = ({size,color,disabledClass}) => {
  return <div className={disabledClass ? '' : styles.loadingWrapper}>
    <GridLoader
      css={styles.override}
      size={size || 15}
      color={color || '#4640DF'}
    />
  </div>
};
