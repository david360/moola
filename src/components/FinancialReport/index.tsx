import * as React from "react";
import * as styles from './styles.module.sass';
import {hot} from "react-hot-loader";
import {
    IAddExpense,
    IAddIncome,
    IAddSpendingTarget, IEditExpense, IEditIncome,
    IEditSpendingTarget,
    IExpense, IIncome,
    ISpendingTarget,
    IUser,
} from "../../hooks/types";
import AddSpendingTarget from "../AddSpendingTarget";
import AddExpense from "../AddExpense";
import AddIncome from "../AddIncome";
import {useToggleState} from "../../hooks/CustomHooks/useToggleState";
import {getUserImage} from "../../helpers/getUserImage";
import {useContext, useEffect, useState} from "react";
import UserData from "../../contexts/UserData";
import CategoryWithExpense from "./CategoryWithExpense";
import {expensesCategories, incomeCategories, SVG_EXTENSION} from "../../hooks/constants";
import {Moment} from "moment";
import {useNumberWithCommas} from "../../helpers/getTotalAmount";
import currencyToSymbolMap from 'currency-symbol-map/map';
import CurrencyContext from "../../contexts/CurrencyContext";
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import {useMappedSpendingLimit} from "../../hooks/useMapSpendingLimit";

const currenciesMap = Object.keys(currencyToSymbolMap);

interface IProps {
    addSpendingTarget: (ST: IAddSpendingTarget) => void;
    updateSpendingTarget: (ST: ISpendingTarget, newValues: IEditSpendingTarget) => Promise<any>;
    updateIncome: (income: IIncome, newValues: IEditIncome) => Promise<any>;
    updateExpense: (ST: IExpense, newValues: IEditExpense) => Promise<any>;
    addIncome: (ST: IAddIncome) => void;
    addExpense: (ST: IAddExpense) => void;
    spendingTargets: ISpendingTarget[];
    expenses: IExpense[];
    selectedExpense: IExpense | undefined;
    selectedIncome: IIncome | undefined;
    handleLogOut: () => void;
    selectedDate?: Moment;
    accountBalance: number;
    incomeModal: boolean;
    toggleIncome: () => void;
    expenseModal: boolean;
    toggleExpense: () => void;
}

const FinancialReport: React.FC<IProps> = ({
                                               addSpendingTarget, addExpense, accountBalance, selectedDate,
                                               addIncome, expenses, spendingTargets, handleLogOut, selectedIncome,
                                               updateIncome, updateExpense, toggleExpense, toggleIncome, expenseModal, incomeModal,
                                               selectedExpense
                                           }) => {
    const {setCurrency, currency} = useContext(CurrencyContext);
    const expensesCategoriesSelectValues = expensesCategories.map(ec => ({
        ...ec,
        value: {image: ec.name + SVG_EXTENSION}
    }));
    const incomeCategoriesSelectValues = incomeCategories.map(ic => ({
        ...ic,
        value: {image: ic.name + SVG_EXTENSION}
    }));
    const [spendingTargetModal, toggleSpendingTarget] = useToggleState();
    const [userImage, setUserImage] = useState<string>('');
    const {userData} = useContext(UserData);
    const userIdArr = userData?.username.split('.') || ['Anonymous', 'id', 'blockstack'];
    const {mappedCategoriesWithExpenses, success} = useMappedSpendingLimit(selectedDate, expenses, spendingTargets);
    useEffect(() => {
        getUserImage(userData as IUser).then(res => {
            setUserImage(res);
        })
    }, [userData && userData.username]);


    return (
        <div className={styles.container}>
            {spendingTargetModal &&
            <AddSpendingTarget modal={spendingTargetModal}
                               categories={expensesCategoriesSelectValues}
                               toggle={toggleSpendingTarget}
                               addSpendingTarget={addSpendingTarget}/>}
            {incomeModal &&
            <AddIncome modal={incomeModal}
                       updateIncome={updateIncome}
                       selectedIncome={selectedIncome}
                       categories={incomeCategoriesSelectValues}
                       toggle={toggleIncome}
                       addIncome={addIncome}/>}
            {expenseModal &&
            <AddExpense modal={expenseModal}
                        categories={expensesCategoriesSelectValues}
                        updateExpense={updateExpense}
                        toggle={toggleExpense}
                        selectedExpense={selectedExpense}
                        addExpense={addExpense}/>}
            <div className={styles.btnsWrapper}>
                <div className={`${styles.btnWrapper} ${styles.incomeBtn}`}>
                    <button onClick={toggleIncome}>
                        <img src={require('../../assets/icon-add.svg')}/>
                    </button>
                    <p>Income</p>
                </div>
                <div className={`${styles.btnWrapper} ${styles.expenseBtn}`}>
                    <button onClick={toggleExpense}>
                        <img src={require('../../assets/icon-remove.svg')}/>
                    </button>
                    <p>Expense</p>
                </div>
            </div>
            <div className={styles.userInfoWrapper}>
                <div className={styles.userInfo}>
                    <img src={userImage}/>
                    <div className={styles.textWrapper}>
                        <p className={styles.username}>
                            {userData?.profile.name || userData?.username.split('.')[0]}
                            <img onClick={handleLogOut} src={require('../../assets/icon-logout.svg')}/>
                        </p>
                        <p className={styles.userId}>
                            <span>{userIdArr[0]}</span>
                            {userIdArr.slice(1).reduce((acc, c) => acc + '.' + c, '')}
                        </p>
                    </div>
                </div>
                <div className={styles.balanceWrapper}>
                    <p className={styles.balance}>{useNumberWithCommas(accountBalance)}</p>
                    <p className={styles.text}>Account Balance</p>
                </div>
            </div>
            <p className={styles.title}>Spending <br/> Limit</p>
            <div className={styles.splitter}/>
            <div className={styles.categories}>
                {
                    (success && mappedCategoriesWithExpenses.length > 0) ? mappedCategoriesWithExpenses.map(cat => {
                        return <CategoryWithExpense key={cat.id} data={cat}/>
                    }) : <img src={require('../../assets/icon-no-expenses.svg')}/>
                }
            </div>
            <div className={styles.buttonsWrapper}>
                <button onClick={toggleSpendingTarget}>
                    + Set/Edit Spending Limit
                </button>
                <UncontrolledDropdown direction={'down'}>
                    <DropdownToggle className={styles.dd}>
                        Currency ({currency})
                    </DropdownToggle>
                    <DropdownMenu className={styles.ddMenu}>
                        {currenciesMap.map(c => {
                            const handleSetCurrency = () => setCurrency(c);
                            return (
                                <DropdownItem
                                    key={c}
                                    onClick={handleSetCurrency}
                                    className={styles.item}>
                                    {c}
                                </DropdownItem>
                            )
                        })}
                    </DropdownMenu>
                </UncontrolledDropdown>
            </div>
        </div>
    );
};

export default hot(module)(FinancialReport);
