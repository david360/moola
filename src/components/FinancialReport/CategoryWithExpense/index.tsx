import * as React from "react";
import * as styles from './styles.module.sass';
import {hot} from "react-hot-loader";
import {IMappedCategoryWithExpense} from "../../../hooks/types";
import {Progress} from 'reactstrap';
import {getExpensesCategoryPath} from "../../../helpers/getCategoryPath";
import {useNumberWithCommas} from "../../../helpers/getTotalAmount";

interface IProps {
    data: IMappedCategoryWithExpense;
}

const CategoryWithExpense: React.FC<IProps> = ({data}) => {
    const expense = useNumberWithCommas(data.limit || '');
    return (
        <div className={styles.container}>
            <div className={styles.cat}>
                <img src={getExpensesCategoryPath(data.image)}/>
                <p>{data.name}</p>
            </div>
            <div className={styles.info}>
                <p className={styles.expense}>
                    {useNumberWithCommas(data.expense || 0)}
                    {data.limit ? ('/' + expense) : ''}
                </p>
                <Progress
                    className={styles.progress}
                    barClassName={(data.expense || 0) > (data.limit || 0) ? styles.redBar : styles.bar}
                    value={((data.expense || 0) / (data.limit || 0)) * 100}/>
            </div>
        </div>
    );
};

export default hot(module)(CategoryWithExpense);
