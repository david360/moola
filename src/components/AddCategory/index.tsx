import * as React from "react";
import * as styles from './styles.module.sass';
import {hot} from "react-hot-loader";
import {Modal, ModalBody} from "reactstrap";
import {Dispatch} from "redux";
import {passOnFormValues as passOnFormValuesRequest} from "../../redux-store/actions";
import {Field, InjectedFormProps, reduxForm} from "redux-form";
import {connect} from "react-redux";
import {FormEvent} from "react";
import {LoadingPageInfo} from "../LoadingPageInfo";
import {isEmpty, required} from "../../redux-store/validators";
import {useProgressState} from "../../hooks/CustomHooks/useProgressState";
import {RenderTextAreaGenericField} from "../FormElements/RenderTextAreaGenericField";
import {CustomSelect} from "../FormElements/select";

export interface IDispatchProps {
    passOnFormValues: (fn) => void
}

interface IProps extends InjectedFormProps, IDispatchProps {
    modal: boolean;
    toggle: () => void;
    addCategory: (cat: any) => Promise<any>;
}

const AddCategory: React.FC<IProps> = ({toggle, modal, addCategory, valid, dirty, passOnFormValues, touch}) => {
    const {setFailure, setSuccess, setLoading, loading} = useProgressState();
    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();
        touch(
            'select',
            'name',
        );
        if (valid && dirty) {
            setLoading();
            passOnFormValues(async ({addCategoryForm}) => {
                const {name, select} = addCategoryForm.values;
                addCategory({name, image: select.value.image}).then(() => {
                    setSuccess();
                    toggle();
                }).catch(setFailure);
            });
        }
    };
    return (
        <Modal isOpen={modal} toggle={toggle} centered={true} contentClassName={styles.content}
               modalClassName={styles.outline}>
            <ModalBody className={styles.body}>
                <div className={styles.container}>
                    <p className={styles.title}>Category</p>
                    <div className={styles.separator}/>
                    <Field
                        name='name'
                        type='text'
                        placeholder='Category Name'
                        component={RenderTextAreaGenericField as any}
                        validate={[required, isEmpty]}
                    />
                    <div className={styles.separator}/>
                    <Field
                        name='select'
                        component={CustomSelect as any}
                        options={[{
                            id: '1',
                            value: {image: 'home.svg'}
                        }, {
                            id: '2',
                            value: {image: 'home.svg'}
                        }, {
                            id: '3',
                            value: {image: 'home.svg'}
                        }, {
                            id: '4',
                            value: {image: 'home.svg'}
                        }, {
                            id: '5',
                            value: {image: 'home.svg'}
                        }, {
                            id: '6',
                            value: {image: 'home.svg'}
                        }, {
                            id: '7',
                            value: {image: 'home.svg'}
                        }, {
                            id: '8',
                            value: {image: 'home.svg'}
                        },]}
                        validate={[required]}
                    />
                    <button disabled={loading} onClick={handleSubmit} className={styles.btn}>
                        {loading ? <>
                            <p>Saving</p>
                            <LoadingPageInfo color='white' size={4} disabledClass={true}/>
                        </> : '+ Save Category'}
                    </button>
                </div>
            </ModalBody>
        </Modal>
    );
};
const mapDispatchToProps = (dispatch: Dispatch) => ({
    passOnFormValues: (fn) => dispatch(passOnFormValuesRequest(fn)),
});

const AddCategoryForm = reduxForm({
    form: 'addCategoryForm',
})(AddCategory as any) as any;

export default hot(module)(connect(null, mapDispatchToProps)(AddCategoryForm) as any);
