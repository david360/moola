import * as React from "react";
import * as styles from './styles.module.sass';
import {hot} from "react-hot-loader";

interface IProps {
    handleSignIn: () => void;
}

const Login: React.FC<IProps> = ({handleSignIn}) => {
    return (
        <div className={styles.container}>
            <div className={styles.content}>
                <div className={styles.wrapper}>
                    <img src={require('../../assets/login-icon.svg')}/>
                    <p className={styles.title}>
                        Spend better,<br/>
                        save better
                    </p>
                    <p className={styles.text}>
                    Keep your income and expenses all in one place securely with Mulla. Mulla 
                    is an expense tracker that allows you to pick spending and income categories 
                    that highlight your daily income and outcome with ease. Start making budgets 
                    on how you spend and save more in the long run.
                    </p>
                    <div className={styles.buttonWrapper}>
                        <button className={styles.button} onClick={handleSignIn}>Start For Free</button>
                        <p>Bank and Card linking will be coming soon!<br/> Stay Tuned!</p>
                    </div>
                </div>
                <img src={require('../../assets/login-image.svg')}/>
            </div>
            <div className={styles.footer}>
                <a href='https://twitter.com/MullaFinance' target='_blank' className={styles.item}>Twitter</a>
                {/*<a  target='_blank' className={styles.item}>Facebook</a>*/}
                <a href='mailto:mulla.finance@outlook.com' className={styles.item}>Email</a>
                <a href='https://angel.co/company/mulla-1' target='_blank' className={styles.item}>Angelist</a>
                {/*<a  target='_blank' className={styles.item}>Product Hunt</a>*/}
                <a  target='_blank' href='https://https://www.mulla.finance//legal.txt' className={styles.item}>Privacy Policy</a>
            </div>
        </div>
    );
};

export default hot(module)(Login);
