import * as React from "react";
import {useEffect, useState} from "react";
import * as styles from './styles.module.sass';
import {hot} from "react-hot-loader";
import DatePicker from "../DatePicker";
import {
    DAY,
    IExpense, IExpenseTransactionWithCategory,
    IIncome,
    IIncomeTransactionWithCategory,
} from "../../hooks/types";
import moment, {Moment} from "moment";
import {getMaxDay, isMomentDaysEqual} from "../../helpers/momentFunctions";
import {expensesCategories, incomeCategories, SVG_EXTENSION} from "../../hooks/constants";
import {getExpensesCategoryPath, getIncomeCategoryPath} from "../../helpers/getCategoryPath";
import {useProgressState} from "../../hooks/CustomHooks/useProgressState";
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import {useNumberWithCommas} from "../../helpers/getTotalAmount";

interface IProps {
    selectedMonth: Moment;
    filteredIncomes: IIncome[];
    filteredExpenses: IExpense[];
    deleteExpense: (expense: IExpense) => void;
    deleteIncome: (expense: IIncome) => void;
    handleEditIncome: (income: IIncome) => void;
    handleEditExpense: (expense: IIncome) => void;
}

const TransactionHistory: React.FC<IProps> = ({
                                                  selectedMonth, filteredIncomes, filteredExpenses,
                                                  deleteIncome, deleteExpense, handleEditIncome, handleEditExpense
                                              }) => {
    const [selectedDate, setSelectedDate] = useState();
    const [dailyFilteredTransactions, setDailyFilteredTransactions] = useState<Array<IIncomeTransactionWithCategory | IExpenseTransactionWithCategory>>([]);
    const {success, setSuccess, setLoading} = useProgressState();
    useEffect(() => {
        setSelectedDate(getMaxDay(moment(selectedMonth)));
    }, [selectedMonth]);
    const mappedIncomeCategories = incomeCategories.reduce((acc, ex) => ({
        ...acc,
        [ex.id]: {...ex, image: ex.name + SVG_EXTENSION}
    }), {});
    const mappedExpensesCategories = expensesCategories.reduce((acc, ex) => ({
        ...acc,
        [ex.id]: {...ex, image: ex.name + SVG_EXTENSION}
    }), {});
    useEffect(() => {
        if (selectedDate) {
            setLoading();
            const dailyFilteredIncomes: IIncomeTransactionWithCategory[] = filteredIncomes
                .filter(item => isMomentDaysEqual(moment(item.createdAt), moment(selectedDate)))
                .map(item => ({...mappedIncomeCategories[item.categoryId], ...item, isIncome: true}));
            const dailyFilteredExpenses: IExpenseTransactionWithCategory[] = filteredExpenses
                .filter(item => isMomentDaysEqual(moment(item.createdAt), moment(selectedDate)))
                .map(item => ({...mappedExpensesCategories[item.categoryId], ...item}));
            setDailyFilteredTransactions([...dailyFilteredExpenses, ...dailyFilteredIncomes]
                .sort((a, b) => a.createdAt - b.createdAt))
            setTimeout(setSuccess);
        }
    }, [selectedDate, filteredIncomes, filteredExpenses]);

    return (
        <div className={styles.container}>
            <div className={styles.headerWrapper}>
                <p>Transaction History</p>
                <DatePicker
                    onDateChange={setSelectedDate}
                    minDate={moment(selectedMonth).startOf('month')}
                    maxDate={getMaxDay(selectedMonth)}
                    currentDate={selectedDate}
                    mode={DAY}/>
            </div>
            <div className={styles.transactions}>
                {
                    (success && dailyFilteredTransactions.length > 0) ? dailyFilteredTransactions.map(transaction => {
                        return <Transaction key={transaction.id}
                                            handleEditIncome={handleEditIncome}
                                            deleteExpense={deleteExpense}
                                            deleteIncome={deleteIncome}
                                            handleEditExpense={handleEditExpense}
                                            transaction={transaction}/>
                    }) : <img style={{margin: 'auto'}} src={require('../../assets/icon-no-history.svg')}/>
                }
            </div>
        </div>
    );
};

export default hot(module)(TransactionHistory);

const Transaction: React.FC<{
    transaction, deleteIncome,
    deleteExpense, handleEditIncome, handleEditExpense
}> = ({
          transaction, deleteExpense,
          deleteIncome, handleEditIncome,handleEditExpense
      }) => {
    const handleDeleteTransaction = () => (transaction.isIncome ? deleteIncome : deleteExpense)(transaction);
    const handleEditTransaction = () => (transaction.isIncome ? handleEditIncome : handleEditExpense)(transaction);

    return (
        <div className={styles.transaction}>
            <div className={styles.imageWrapper}>
                <img src={(transaction.isIncome ? getIncomeCategoryPath :
                    getExpensesCategoryPath)(transaction.image)} className={styles.catImage}/>
            </div>
            <p className={styles.catName}>{transaction.name}</p>
            <p className={styles.description}>{transaction.description || 'No description added!'}</p>
            <p style={{color: transaction.isIncome ? '#23D796' : '#FF4D00'}}
               className={styles.amount}>
                {useNumberWithCommas(+transaction.amount)}
            </p>
            <p className={styles.date}>
                {moment(transaction.createdAt).format('YYYY-MM-D hh:mm')}
            </p>
            <UncontrolledDropdown direction={'down'}>
                <DropdownToggle className={styles.dd}>
                    <div className={styles.options}>
                        <img className={styles.edit}
                             src={require('../../assets/icon-edit.svg')}/>
                    </div>
                </DropdownToggle>
                <DropdownMenu className={styles.ddMenu}>
                    <DropdownItem
                        onClick={handleEditTransaction}
                        className={styles.item}>
                        Edit Transaction
                    </DropdownItem>
                    <DropdownItem
                        onClick={handleDeleteTransaction}
                        className={styles.itemRed}>
                        Delete Transaction
                    </DropdownItem>
                </DropdownMenu>
            </UncontrolledDropdown>
        </div>
    )
};