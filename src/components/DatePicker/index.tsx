import * as React from "react";
import * as styles from './styles.module.sass';
import * as moment from "moment";
import {Moment} from "moment";
import {useEffect} from "react";
import {DAY, day, MONTH, month} from "../../hooks/types";

const immutableMoment = (m: Moment) => moment(m);


interface IProps {
    onDateChange: (date: Moment) => any;
    minDate: Moment;
    maxDate: Moment;
    currentDate: Moment;
    mode: day | month;
}

export const isBetweenMinNMax = (min: Moment, max: Moment, nextDate: Moment) => {
    const isSmallerThanMax = max.diff(nextDate) >= 0;
    const isLargerThanMin = min.diff(nextDate) <= 0;
    return isSmallerThanMax && isLargerThanMin;
};

const constructTapFactory = (keyCode: number) => {
    return (cb: () => any) => {
        const onTapCallBack = (ev) => {
            if (ev.keyCode === keyCode) {
                cb();
            }
        };
        document.addEventListener('keydown', onTapCallBack);
        return () => document.removeEventListener('keydown', onTapCallBack);
    }
};

enum ArrowKeys {
    left = 37,
    right = 39,
}

const DatePicker: React.FC<IProps> = props => {
    const {currentDate, maxDate, minDate, onDateChange, mode} = props;
    const immutableDate = () => immutableMoment(currentDate);

    let listeners: Array<() => any> = [];
    useEffect(() => {
        listeners = [
            constructTapFactory(ArrowKeys.left)(onMonthBackClicked),
            constructTapFactory(ArrowKeys.right)(onMonthNextClicked),
        ];

        return () => listeners.forEach(cb => cb());
    });
    const isNextMonthAvailable = () => validateDateChange(immutableDate().add(1, mode));
    const isPastMonthAvailable = () => validateDateChange(immutableDate().subtract(1, mode));

    const setCurrentDate = (date: Moment) => onDateChange(date);


    const onMonthBackClicked = () =>
        isPastMonthAvailable() && setCurrentDate(immutableDate().subtract(1, mode));

    const onMonthNextClicked = () =>
        isNextMonthAvailable() && setCurrentDate(immutableDate().add(1, mode));

    const validateDateChange = (nextDate: Moment) => isBetweenMinNMax(minDate, maxDate, nextDate);

    const fontColor = mode === DAY ? '#A0A0A0' : '#171725';
    const fontWeight = mode === DAY ? 500 : 900;
    const fontSize = mode === DAY ? 13 : 24;
    const buttonsFontSize = mode === DAY ? 14 : 20;
    return (
        <div className={styles.wrapper}>
            <div className={styles.monthWrapper}>
                <button style={{color: fontColor,fontSize: buttonsFontSize}} disabled={!isPastMonthAvailable()} onClick={onMonthBackClicked}
                        className={styles.back}>{'<'}</button>
                <div style={{color: fontColor,fontWeight,fontSize}} className={styles.month}>
                    {mode === DAY ? moment(currentDate).format('D') + ' ' : ''}
                    {moment(currentDate).format('MMM')}
                    {mode === MONTH ? ' ' + moment(currentDate).format('YYYY') : ''}
                </div>
                <button style={{color: fontColor,fontSize: buttonsFontSize}} disabled={!isNextMonthAvailable()} onClick={onMonthNextClicked}
                        className={styles.next}>{'>'}</button>
            </div>

        </div>
    );
};

export default DatePicker;
