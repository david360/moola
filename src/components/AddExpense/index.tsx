import * as React from "react";
import * as styles from './styles.module.sass';
import {hot} from "react-hot-loader";
import {Modal, ModalBody} from "reactstrap";
import {Dispatch} from "redux";
import {passOnFormValues as passOnFormValuesRequest} from "../../redux-store/actions";
import {Field, InjectedFormProps, reduxForm} from "redux-form";
import {connect} from "react-redux";
import {FormEvent} from "react";
import {LoadingPageInfo} from "../LoadingPageInfo";
import {isEmpty, required} from "../../redux-store/validators";
import {useProgressState} from "../../hooks/CustomHooks/useProgressState";
import {CustomSelect} from "../FormElements/select";
import {RenderGenericField} from "../FormElements/RenderGenericField";
import {IAddExpense, ICategory, IEditExpense, IExpense} from "../../hooks/types";
import {RenderTextAreaGenericField} from "../FormElements/RenderTextAreaGenericField";
import {useNumberWithCommas} from "../../helpers/getTotalAmount";
import {useEffect} from "react";
import {expensesCategories} from "../../hooks/constants";

export interface IDispatchProps {
    passOnFormValues: (fn) => void
}

interface IProps extends InjectedFormProps, IDispatchProps {
    modal: boolean;
    categories: ICategory[];
    updateExpense: (income: IExpense, newValues: IEditExpense) => Promise<any>;
    selectedExpense: IExpense;
    toggle: () => void;
    addExpense: (expense: IAddExpense) => Promise<any>;
}

const AddExpense: React.FC<IProps> = ({
                                                 toggle, modal, addExpense, valid,
                                                 dirty, passOnFormValues, touch, categories,
    selectedExpense,initialize,updateExpense
                                             }) => {
    const {setFailure, setSuccess, setLoading, loading} = useProgressState();
    useEffect(() => {
        const initializeForm = async () => {
            if (selectedExpense) {
                const selectedCategory = expensesCategories.filter(c => c.id === selectedExpense.categoryId.toString());
                initialize({...selectedExpense,category: selectedCategory[0]});
            }
        };
        initializeForm();
    }, [selectedExpense]);
    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();
        touch(
            'amount',
            'category',
        );
        if (valid && dirty) {
            setLoading();
            passOnFormValues(async ({addExpenseForm}) => {
                const {category, amount,description} = addExpenseForm.values;
                if(selectedExpense){
                    updateExpense(selectedExpense,{categoryId: category.id, amount,description}).then(() => {
                        setSuccess();
                        toggle();
                    }).catch(setFailure);
                }else{
                    addExpense({categoryId: category.id, amount,description}).then(() => {
                        setSuccess();
                        toggle();
                    }).catch(setFailure);
                }
            });
        }
    };

    return (
        <Modal isOpen={modal} toggle={toggle} size={'lg'} centered={true} contentClassName={styles.content}
               modalClassName={styles.outline}>
            <ModalBody className={styles.body}>
                <div className={styles.container}>
                    <Field
                        name='amount'
                        type='number'
                        placeholder={useNumberWithCommas('00.00')}
                        icon={<p style={{fontSize: 58, fontWeight: 900, color: '#F35B51'}}>-</p>}
                        component={RenderGenericField as any}
                        validate={[required, isEmpty]}
                    />
                    <div className={styles.separator}/>
                    <Field
                        name='description'
                        type='text'
                        placeholder='Description (Optional)'
                        component={RenderTextAreaGenericField as any}
                    />
                    <div className={styles.separator}/>
                    <Field
                        name='category'
                        rowItems={5}
                        component={CustomSelect as any}
                        options={categories}
                        validate={[required]}
                    />
                    <button disabled={loading} onClick={handleSubmit} className={styles.btn}>
                        {loading ? <>
                            <p>Saving</p>
                            <LoadingPageInfo color='white' size={4} disabledClass={true}/>
                        </> : '+ Record Transaction'}
                    </button>
                </div>
            </ModalBody>
        </Modal>
    );
};
const mapDispatchToProps = (dispatch: Dispatch) => ({
    passOnFormValues: (fn) => dispatch(passOnFormValuesRequest(fn)),
});

const AddExpenseForm = reduxForm({
    form: 'addExpenseForm',
})(AddExpense as any) as any;

export default hot(module)(connect(null, mapDispatchToProps)(AddExpenseForm) as any);
