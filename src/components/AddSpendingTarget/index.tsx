import * as React from "react";
import * as styles from './styles.module.sass';
import {hot} from "react-hot-loader";
import {Modal, ModalBody} from "reactstrap";
import {Dispatch} from "redux";
import {passOnFormValues as passOnFormValuesRequest} from "../../redux-store/actions";
import {Field, InjectedFormProps, reduxForm} from "redux-form";
import {connect} from "react-redux";
import {FormEvent} from "react";
import {LoadingPageInfo} from "../LoadingPageInfo";
import {isEmpty, required} from "../../redux-store/validators";
import {useProgressState} from "../../hooks/CustomHooks/useProgressState";
import {CustomSelect} from "../FormElements/select";
import {RenderGenericField} from "../FormElements/RenderGenericField";
import {IAddSpendingTarget, ICategory} from "../../hooks/types";
import {useNumberWithCommas} from "../../helpers/getTotalAmount";

export interface IDispatchProps {
    passOnFormValues: (fn) => void
}

interface IProps extends InjectedFormProps, IDispatchProps {
    modal: boolean;
    categories: ICategory[];
    toggle: () => void;
    addSpendingTarget: (cat: IAddSpendingTarget) => Promise<any>;
}

const AddSpendingTarget: React.FC<IProps> = ({
                                                 toggle, modal, addSpendingTarget, valid,
                                                 dirty, passOnFormValues, touch, categories
                                             }) => {
    const {setFailure, setSuccess, setLoading, loading} = useProgressState();
    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();
        touch(
            'amount',
            'category',
        );
        if (valid && dirty) {
            setLoading();
            passOnFormValues(async ({addSpendingTargetForm}) => {
                const {category, amount} = addSpendingTargetForm.values;
                addSpendingTarget({categoryId: category.id, amount}).then(() => {
                    setSuccess();
                    toggle();
                }).catch(setFailure);
            });
        }
    };
    return (
        <Modal isOpen={modal} toggle={toggle} size={'lg'} centered={true} contentClassName={styles.content}
               modalClassName={styles.outline}>
            <ModalBody className={styles.body}>
                <div className={styles.container}>
                    <Field
                        name='amount'
                        type='number'
                        placeholder={useNumberWithCommas('00.00')}
                        icon={<img src={require('../../assets/icon-spending-target.svg')}/>}
                        component={RenderGenericField as any}
                        validate={[required, isEmpty]}
                    />
                    <div className={styles.separator}/>
                    <Field
                        name='category'
                        rowItems={5}
                        component={CustomSelect as any}
                        options={categories}
                        validate={[required]}
                    />
                    <button disabled={loading} onClick={handleSubmit} className={styles.btn}>
                        {loading ? <>
                            <p>Saving</p>
                            <LoadingPageInfo color='white' size={4} disabledClass={true}/>
                        </> : '+ Spending Target'}
                    </button>
                </div>
            </ModalBody>
        </Modal>
    );
};
const mapDispatchToProps = (dispatch: Dispatch) => ({
    passOnFormValues: (fn) => dispatch(passOnFormValuesRequest(fn)),
});

const AddSpendingTargetForm = reduxForm({
    form: 'addSpendingTargetForm',
})(AddSpendingTarget as any) as any;

export default hot(module)(connect(null, mapDispatchToProps)(AddSpendingTargetForm) as any);
