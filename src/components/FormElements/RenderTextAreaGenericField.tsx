import * as React from "react";
import * as styles from './styles.module.sass';
import {WrappedFieldProps} from 'redux-form';

interface IProps {
    label: string;
    type: string;
    placeholder: string;
    className?: string;
    viewOnly?: boolean;
    labelContent: React.ReactChild;
}


export class RenderTextAreaGenericField extends React.Component<WrappedFieldProps & IProps> {
    public render() {
        const {
            input,
            meta: {touched, error, warning},
            placeholder,
            viewOnly,
            labelContent,
            className
        } = this.props;
        return (
            <div className={`${styles.textAreaWrapper} ${className || ''}`}>
                <label htmlFor={input.name} className={styles.label}>
                    {labelContent}
                  {touched && ((error && <span className={styles.formError}>{error}</span>) || (warning &&
                    <span>{warning}</span>))}
                </label>
                <textarea disabled={viewOnly} autoComplete='off' className={styles.textArea} id={input.name} {...input}
                          placeholder={placeholder}/>
            </div>
        );
    }
}
