import * as React from "react";
import * as styles from './styles.module.sass';
import {WrappedFieldProps} from 'redux-form';

interface IProps {
    label: string;
    type: string;
    placeholder: string;
    validationInRow?: boolean;
    autoFocus?: boolean;
    viewOnly?: boolean;
    className?: string;
    noValidationMessages?: boolean;
    icon: React.ReactChild;
    onBlur?: (e: React.FocusEvent<HTMLInputElement>) => void;
}

export class RenderGenericField extends React.Component<WrappedFieldProps & IProps> {
    private input: HTMLInputElement;

    public focus = () => this.input && this.input.focus();
    public handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
        const {onBlur, input} = this.props;
        input.onBlur(e);
        if (onBlur) {
            onBlur(e);
        }
    };

    public render() {
        const {
            input,
            viewOnly,
            validationInRow,
            label,
            type,
            placeholder,
            meta: {touched, error, warning},
            autoFocus,
            noValidationMessages,
            className
        } = this.props;
        if (autoFocus) {
            setTimeout(this.focus, 1000);

        }
        return (
            <div style={{
                flexDirection: validationInRow ? 'row-reverse' : 'column',
                alignItems: validationInRow ? 'center' : 'flex-start'
            }}
                 className={`${styles.inputWrapper} ${className || ''}`}>
                <label htmlFor={input.name}
                       style={!validationInRow ? {width: '100%'} : {margin: label ? '0 0 1rem' : 0}}
                       className={styles.label}>
                    <span>{label}</span>
                    {!noValidationMessages && touched && ((error &&
                        <span style={{margin: validationInRow ? '0 1rem' : 0}}
                              className={styles.formError}>{error}</span>) || (warning &&
                        <span>{warning}</span>))}
                </label>
                <div className={styles.inputContainer}>
                    <input disabled={viewOnly} autoComplete='off'
                           placeholder={placeholder} className={styles.input} id={input.name} {...input}
                           ref={this.inputRef}
                           onBlur={this.handleBlur}
                           type={type}/>
                </div>
            </div>
        );
    }

    private inputRef = (elem: HTMLInputElement | null) => {
        if (elem) {
            this.input = elem;
        }
    };
}
