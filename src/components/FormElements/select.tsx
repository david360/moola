import * as React from "react";
import * as styles from './styles.module.sass';
import {noop} from "redux-saga/utils";
import {WrappedFieldProps} from "redux-form";
import {getExpensesCategoryPath, getIncomeCategoryPath} from "../../helpers/getCategoryPath";

export interface IDropDownItemData<OptionType = any> {
    label: string;
    value: OptionType;
}

export interface ISelectItemData<ValueType = any> {
    name: string;
    id: string;
    value: ValueType;
}

interface ISelectItemProps {
    item: ISelectItemData;
    isIncome?: boolean;
}

const SelectItem: React.FunctionComponent<ISelectItemProps> = ({item, isIncome}) => {
    return (
        <img className={styles.selectItem}
             src={(isIncome ? getIncomeCategoryPath : getExpensesCategoryPath)(item.value.image)}/>
    );
};

interface IRenderSelectProps extends WrappedFieldProps {
    options: ISelectItemData[];
    rowItems: number;
    isIncome?: boolean;
    renderSelectItem?: (item: ISelectItemData) => React.ReactChild;
}

const SelectItemWrapper: React.FunctionComponent<{
    onSelect: (value: any) => void;
    isIncome?: boolean;
    value: any;
    selected: boolean,
    rowItems: number
}> = (props) => {
    const onSelect = () => props.onSelect(props.value);
    return (
        <div className={styles.selectedItemContainer} style={{width: `${(1 / props.rowItems) * 100}%`}}>
            <div style={{
                background: props.isIncome ? 'rgba(35, 215, 105,.1)' : 'rgba(243,91,81,.1)',
                boxShadow: props.selected ? (props.isIncome ? '0 0 15px rgba(35, 215, 105, 1)':'0 0 15px rgba(243, 91, 81, 1)') : 'none'
            }}
                 className={styles.selectItemWrapper}
                 onClick={props.selected ? noop : onSelect}>{props.children}</div>
            {props.value.name && <p>{props.value.name}</p>}
        </div>
    );
};

export const CustomSelect: React.FunctionComponent<IRenderSelectProps> = (props) => {
    const {
        options, renderSelectItem, input,
        meta: {touched, error, warning},
        rowItems,
        isIncome
    } = props;

    const handleChange = (v) => {
        input.onChange(v);
    };
    const defaultRenderSelectItem = (option) => <SelectItem isIncome={isIncome} item={option}/>;

    return (
        <div className={styles.selectContainer}>
            {touched && ((error &&
                <span className={styles.formError}>{error}</span>) || (warning &&
                <span>{warning}</span>))}
            <div className={styles.selectWrapper}>
                {
                    options.map((option, index) =>
                        <SelectItemWrapper key={index}
                                           selected={input.value && (option.id === input.value.id)}
                                           value={option}
                                           isIncome={isIncome}
                                           rowItems={rowItems}
                                           onSelect={handleChange}>
                            {(renderSelectItem || defaultRenderSelectItem)(option)}
                        </SelectItemWrapper>
                    )
                }
            </div>
        </div>
    );
};
