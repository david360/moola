import * as React from "react";
import * as styles from './styles.module.sass';
import {hot} from "react-hot-loader";
import DatePicker from "../DatePicker";
import {maxDate, minDate} from "../../hooks/constants";
import moment, {Moment} from "moment";
import {IExpense, IIncome, MONTH} from "../../hooks/types";
import TransactionHistory from "../TransactionHistory";
import {getTotalAmount, useNumberWithCommas} from "../../helpers/getTotalAmount";
import BalanceStatChart from "../BalanceStatsChart";

interface IProps {
    selectedDate: Moment;
    setSelectedDate: (date: Moment) => void;
    deleteExpense: (expense: IExpense) => void;
    deleteIncome: (income: IIncome) => void;
    handleEditIncome: (income: IIncome) => void;
    handleEditExpense: (expense: IExpense) => void;
    filteredIncomes: IIncome[];
    filteredExpenses: IExpense[];
}

const mappedTransactionWithDays = (arr, initialBalanceMap) => {
    return arr.reduce((acc, i) => {
        const currentDay = +moment(+i.createdAt).endOf('day').format('x');
        return {...acc, [currentDay]: acc[currentDay] + +i.amount}
    }, initialBalanceMap);
};

const FinancialMetric: React.FC<IProps> = ({
                                               selectedDate, setSelectedDate, deleteExpense, deleteIncome, handleEditExpense,
                                               filteredExpenses, filteredIncomes, handleEditIncome
                                           }) => {

    const filteredAccountIncomes = getTotalAmount(filteredIncomes);
    const filteredAccountExpenses = getTotalAmount(filteredExpenses);
    const initialBalanceMap = new Array(moment(selectedDate).daysInMonth()).fill(0).reduce((acc, i, index) => {
        return {...acc, [+moment(selectedDate).startOf('month').add(index, 'day').endOf('day')]: 0};
    }, {});
    const mappedIncomeByDay = mappedTransactionWithDays(filteredIncomes, initialBalanceMap);
    const mappedExpenseByDay = mappedTransactionWithDays(filteredExpenses, initialBalanceMap);
    const mappedInitialBalance = Object.keys(initialBalanceMap).reduce((acc, key) => {
        return [...acc, {y: Math.ceil(mappedIncomeByDay[key] - mappedExpenseByDay[key]), x: +key}];
    }, []);
    return (
        <div className={styles.container}>
            <div className={styles.headerWrapper}>
                <p>Financial Metric</p>
                {selectedDate && <DatePicker onDateChange={setSelectedDate}
                                             minDate={minDate}
                                             mode={MONTH}
                                             maxDate={maxDate.endOf('month')}
                                             currentDate={selectedDate}/>}
            </div>
            <div className={styles.cards}>
                <div className={styles.card}>
                    <p className={styles.label}>Income</p>
                    <p className={styles.value1}>{useNumberWithCommas(filteredAccountIncomes)}</p>
                </div>
                <div className={styles.card}>
                    <p className={styles.label}>Expense</p>
                    <p className={styles.value2}>{useNumberWithCommas(filteredAccountExpenses)}</p>
                </div>
                <div className={styles.card}>
                    <p className={styles.label}>Balance</p>
                    <p className={styles.value3}>{useNumberWithCommas(filteredAccountIncomes - filteredAccountExpenses)}</p>
                </div>
            </div>
            <BalanceStatChart selectedMonth={selectedDate} chartSeries={mappedInitialBalance}/>
            {selectedDate && <TransactionHistory
                deleteExpense={deleteExpense}
                handleEditIncome={handleEditIncome}
                handleEditExpense={handleEditExpense}
                deleteIncome={deleteIncome}
                filteredIncomes={filteredIncomes}
                filteredExpenses={filteredExpenses}
                selectedMonth={selectedDate}
            />}
        </div>
    );
};

export default hot(module)(FinancialMetric);
