import * as React from 'react';
import * as styles from './App.module.sass';
import Notifications, {notify} from 'react-notify-toast';
import {useAuth} from "./hooks/useAuth";
import UserData from './contexts/UserData';
import {BrowserRouter, Switch, Redirect} from "react-router-dom";
import {LoadingPageInfo} from "./components/LoadingPageInfo";
import Login from "./components/Login";
import {useItemsCrud} from "./hooks/useItemsCrud";

import {
    IAddExpense,
    IAddIncome,
    IAddSpendingTarget, IExpense, IIncome,
} from "./hooks/types";
import FinancialReport from "./components/FinancialReport";
import {
    CURRENCY_FILENAME,
    EXPENSES_FILENAME,
    INCOMES_FILENAME,
    maxDate,
    SPENDING_TARGET_FILENAME
} from "./hooks/constants";
import {useEffect, useState} from "react";
import FinancialMetric from "./components/FinancialMetric";
import {getTotalAmount} from "./helpers/getTotalAmount";
import moment from "moment";
import CurrencyContext from "./contexts/CurrencyContext";
import {getFile, putFile} from "blockstack/lib";
import {useToggleState} from "./hooks/CustomHooks/useToggleState";


const AuthenticatedWrapper = (props) => {
    const [selectedDate, setSelectedDate] = useState();
    const [incomeModal, toggleIncome] = useToggleState();
    const [expenseModal, toggleExpense] = useToggleState();
    const [selectedIncome,setSelectedIncome] = useState<IIncome | undefined>();
    const [selectedExpense,setSelectedExpense] = useState<IIncome | undefined>();
    const handleEditIncome = (selected: IIncome) => {
      setSelectedIncome(selected);
      toggleIncome();
    };
    const handleEditExpense = (selected: IExpense) => {
      setSelectedExpense(selected);
      toggleExpense();
    };
    const [currency, setCurrency] = useState();
    useEffect(() => {
        getFile(CURRENCY_FILENAME).then((res: any) => {
            setCurrency(JSON.parse(res) || "USD");
        });
    }, []);
    useEffect(() => {
        setSelectedDate(moment(maxDate));
    }, []);
    const handleSetCurrency = (newCur) => {
        const oldCur = currency;
        setCurrency(newCur);
        putFile(CURRENCY_FILENAME, JSON.stringify(newCur)).then(() => {
            notify.show('Currency has been changed successfully!','success')
        }).catch(() => {
            notify.show('Something went wrong!', 'error');
            setCurrency(oldCur);
        });
    };
    const {
        items: spendingTargets,
        addItem: addSpendingTarget,
        success: spendingTargetsSuccess,
        updateItem: updateSpendingTarget,
    } = useItemsCrud<IAddSpendingTarget>(SPENDING_TARGET_FILENAME, selectedDate);
    const {
        items: expenses,
        success: expensesSuccess,
        filteredItems: filteredExpenses,
        addItem: addExpense,
        updateItem: updateExpense,
        deleteItem: deleteExpense,
    } = useItemsCrud<IAddExpense>(EXPENSES_FILENAME, selectedDate);
    const {
        items: incomes,
        success: incomeSuccess,
        updateItem: updateIncome,
        filteredItems: filteredIncomes,
        addItem: addIncome,
        deleteItem: deleteIncome,
    } = useItemsCrud<IAddIncome>(INCOMES_FILENAME, selectedDate);

    const accountIncome = getTotalAmount(incomes);
    const accountExpenses = getTotalAmount(expenses);
    const accountBalance = accountIncome - accountExpenses;
    return (
        <CurrencyContext.Provider value={{currency, setCurrency: handleSetCurrency}}>
            <>
                <Switch>
                    {props.location.search.includes('authResponse') && <Redirect to={'/'}/>}
                    <>
                        {spendingTargetsSuccess &&
                        expensesSuccess && incomeSuccess && <FinancialReport
                            addExpense={addExpense}
                            addIncome={addIncome}
                            handleLogOut={props.handleLogOut}
                            expenseModal={expenseModal}
                            selectedExpense={selectedExpense}
                            selectedIncome={selectedIncome}
                            incomeModal={incomeModal}
                            toggleExpense={toggleExpense}
                            toggleIncome={toggleIncome}
                            accountBalance={accountBalance}
                            updateIncome={updateIncome}
                            updateExpense={updateExpense}
                            expenses={filteredExpenses}
                            spendingTargets={spendingTargets}
                            selectedDate={selectedDate}
                            updateSpendingTarget={updateSpendingTarget}
                            addSpendingTarget={addSpendingTarget}/>}
                        {spendingTargetsSuccess &&
                        expensesSuccess && incomeSuccess && <FinancialMetric
                            deleteExpense={deleteExpense}
                            handleEditIncome={handleEditIncome}
                            handleEditExpense={handleEditExpense}
                            deleteIncome={deleteIncome}
                            filteredExpenses={filteredExpenses}
                            filteredIncomes={filteredIncomes}
                            selectedDate={selectedDate}
                            setSelectedDate={setSelectedDate}/>}
                    </>
                </Switch>
            </>
        </CurrencyContext.Provider>
    );
};

const App: React.FC = () => {
    const {userData, handleSignIn, isAuthenticated, loading, setUserData, handleLogOut} = useAuth();
    return (
        <BrowserRouter>
            <UserData.Provider value={{userData, setUserData}}>
                <div className={styles.App}>
                    <div style={{fontSize: '2rem'}}>
                        <Notifications options={{zIndex: 2000}}/>
                    </div>
                    <Switch>
                        {(loading || isAuthenticated === undefined) ?
                            <LoadingPageInfo/>
                            : (isAuthenticated && userData) ?
                                <AuthenticatedWrapper handleLogOut={handleLogOut}/>
                                :
                                <>
                                    <Login handleSignIn={handleSignIn}/>
                                </>
                        }
                    </Switch>
                </div>
            </UserData.Provider>
        </BrowserRouter>
    );
};

export default App;

