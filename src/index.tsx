// import register from '../plugin-sw-precache/register-service-worker'
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import './index.module.sass';
import './styles.css';
import persistedStore from './redux-store/configureStore';
// import registerServiceWorker from './registerServiceWorker';
import {Provider} from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';

// register();

async function init() {
  const {store} = await persistedStore();

  ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root') as HTMLDivElement
  );
  // registerServiceWorker();
}

init();
