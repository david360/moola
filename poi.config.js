const env = process.env.NODE_ENV;

const config = {
  entry: 'src/index.tsx',
  output: {
    sourceMap: true,
    html: {
      template: './public/index.html',
    },
  },
  envs: {
    PUBLIC_URL: '/',
  },
  devServer: {
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE",
      "Access-Control-Allow-Headers": "Content-Type",
    }
  },
  plugins: [
    {
      resolve: '@poi/plugin-typescript',
      options: {}
    },
    {
      resolve: '@poi/bundle-report',
      options: {}
    },
  ],
};
module.exports = config;
