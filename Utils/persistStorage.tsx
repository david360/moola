export default async function persist() {
  return await navigator.storage && navigator.storage.persist && navigator.storage.persist();
}
