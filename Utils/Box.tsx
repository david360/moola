export default class Box<T> {
  private readonly value: T;
  constructor(v: T) {
    this.value = v;
  }
  public map<S>(f: (v: T) => S): Box<S> {
    return new Box<S>(f(this.value));
  }
  public fold(f: (v: T) => any) {
    return f(this.value);
  }
}
